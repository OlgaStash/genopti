from tkinter import *

class TestGUI:

    def __init__(self):

        myBgColor = '#d5e8e8'
        myInitColor = '#e6f5f5'
        myBtnColor = '#1797ff'
        myTitleColor = '#164452'

        # задаем параметны интерфейса
        self.root = Tk()
        self.root.title("Генетический алгоритм")
        self.root.iconbitmap(bitmap=None)


        # задаем три Frame, в которые будем размещаться виджеты по ветрикали
        self.lFr = LabelFrame(
            self.root, height=600, width=350, bg=myTitleColor,
            padx=5, pady=5
        ).pack(padx=5, pady=5)
        self.population_number_label = Label(
            self.lFr, text="Размер начальной популяции:", font='Helvetica 15',
            bg=myBgColor, fg=myInitColor
        ).pack()
        self.cFr = LabelFrame(
            self.root, height=600, width=350
        ).pack()
        self.rFr = LabelFrame(
            self.root, height=600, width=400, bg=myTitleColor,
        ).pack()
        self.root.mainloop()
