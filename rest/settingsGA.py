class gaParams:

    def __init__(self,
                 population_number: int, generations_number: int,
                 mutation_chance: int, cross_chance: int,
                 strategy_choice: int, selection_type: int,
                 cross_operator: int, mutation_operator: int
                 ):

        self.population_number = population_number
        self.generations_number = generations_number
        self.mutation_chance = mutation_chance
        self.cross_chance = cross_chance
        self.strategy_choice = strategy_choice
        self.selection_type = selection_type
        self.cross_operator = cross_operator
        self.mutation_operator = mutation_operator
