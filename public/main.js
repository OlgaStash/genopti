let out = document.getElementsByName('output')[0];

let answer = document.getElementsByName('answer')[0];


function rand(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

// фенотип хромосомы 'ch', то есть расшифровка решения, представляемого хромосомой ch
function decryption(ch) {
    return (8 * ch[0] + 4 * ch[1] + 2 * ch[2] + 1 * ch[3]);
}

// Вычисляем целевую функцию
function objective_function(x) {
    return  x * x + 20 * x - 34;
}

// Быстрая сортировка массива генов
function quick_sort(array, low, high) {
    let i;
    i = low;
    let j;
    j = high;
    let base;
    base = objective_function(decryption(array[Math.trunc((i + j) / 2)]));
    while (i <= j) {
        while (objective_function(decryption(array[i])) > base) ++i;
        while (objective_function(decryption(array[j])) < base) --j;
        if (i <= j) {
            if (i < j) {
                for (let n = 0; n < 4; ++n) {
                    let temp = array[i][n];
                    array[i][n] = array[j][n];
                    array[j][n] = temp;
                }
            }
            i++;
            j--;
        }
    }
    if (j > low)
        quick_sort(array, low, j);
    if (i < high)
        quick_sort(array, i, high);
}


//Случайная селекция
function selection_random() {
    //заполняем родительский пул случайными хромосомами.
    //одновременно выводим отобранные хромомсомы
    let k = 0;  //номер случайно выбранной хромосомы
    out.append("Случайная селекция. Отобранные хромосомы:\r\n\r\n");
    for (let i = 0; i < crossingover_max_amount; ++i) {
        k = rand(population_size);
        for (let j = 0; j < 4; ++j) {
            population[population_size + i][j] = population[k][j];
            if (population[population_size + i][j]) {
                out.append("1 ");
            }
            else {
                out.append("0 ");
            }
        }
        out.append("\r\n");
        if (i % 2 === 1) {
            out.append("\r\n");
        }
    }
}

//Селекция инбридинга
function selection_inbreeding() {
    quick_sort(population, 0, population_size - 1);
    // формируем в популяции случайные близкородственные пары, причем одна и та же хромосома может входить в несколько пар
    let k;//случайная позиция первой хромосомы в паре
    for (let i = population_size; i < population_size + crossingover_max_amount; i += 2) {
        k = rand(population_size - 1);
        for (let j = 0; j < 4; ++j) {
            population[i][j] = population[k][j]; //записываем геном первой хромосомы в паре
            population[i + 1][j] = population[k + 1][j]; //и второй
        }
    }

    out.append("Селекция инбридинга. Отобранные хромосомы:\r\n\r\n");
    for (let i = 0; i < crossingover_max_amount; ++i) {
        for (let j = 0; j < 4; j++) {
            population[population_size + i][j] = population[i][j]
            if (population[population_size + i][j]) {
                out.append("1 ");
            } else {
                out.append("0 ");
            }
        }
        out.append("\r\n");
        if (i % 2 === 1) {
            out.append("\r\n");
        }
    }
}

//Стандартный одноточечный оператор кроссинговера
function standard_single() {
    out.append("Стандартный одноточечный оператор кроссинговера\r\n");
    let l1; //точкa скрещивания
    for (let i = 0; i < crossingover_max_amount; i += 2) {
        //генерация точки скрещивания
        l1 = rand(3);
        out.append("Хромосомы " + (i + 1) + " и " + (i + 2)
            + " : выбрана точка после гена под номером " + (l1 + 1) + "\r\n");
        //обмен генов
        for (let j = 0; j <= l1; ++j) {
            //до точки скрещивания
            child[i][j] = population[population_size + i][j];
            child[i+1][j] = population[population_size + i + 1][j];
        }
        for (let j = l1 + 1; j < 4; ++j) {
            //после точки скрещивания
            child[i+1][j] = population[population_size + i][j];
            child[i][j] = population[population_size + i + 1][j];
        }
    }

    out.append("\r\nПул потомков:\r\n\r\n");
    for (let i = 0; i < crossingover_max_amount; ++i) {
        for (let j = 0; j < 4; ++j) {
            if (child[i][j]) {
                out.append("1 ");
            } else {
                out.append("0 ");
            }
        }
        out.append("\r\n");
        if (i % 2 === 1) out.append("\r\n");
    }

    for (let i = 0; i < crossingover_max_amount; ++i) {
        for (let j = 0; j < 4; ++j) {
            population[population_size + i][j] = child[i][j];
        }
    }
}


//Частично соответствующий одноточечный оператор кроссинговера
function prelevant_single() {
    out.append("Частично соответствующий одноточечный оператор кроссинговера\r\n");
    let tmp = [4];//временная хромосома для хранения генов во время обмена
    let l1, l2;
    let tmpl;//точки скрещивания и переменная для их перестановки
    let b;//логика оператора
    for (let i = population_size; i < population_size + crossingover_max_amount; i += 2) {
        //генерация двух точек скрещивания
        l1 = rand(3);
        l2 = rand(3);
        while (l1 === l2) l2 = rand(3);
        if (l2 < l1) {
            tmpl = l1;
            l1 = l2;
            l2 = tmpl;
        }
        out.append("Хромосомы " + (i) + " и " + (i + 1)
            + " : выбрана точки: " + (l1) + ", " + (l2) + "\r\n");
        //обмен генов
        for (let j = l1 + 1; j <= l2; ++j) {
            tmp[j] = population[i][j];
            population[i][j] = population[i + 1][j];
            population[i + 1][j] = tmp[j];
        }
        //далее проверяем оставшиеся элементы на повторения и заменяем повторяющиеся на первый частично соответствующий
        //для элементов до l1
        for (let j = 0; j <= l1; ++j) {
            //для i-го элемента
            b = l1 + 1;
            //сравниваем текущий элемент с элементами которые мы обменяли пока не найдем повтор или не выйдем за границу
            while (b <= l2 && population[i][j] !== population[i][b]) ++(b);
            //если не вышли за границу, то нашли повтор
            if (b !== l2 + 1) population[i][j] = population[i + 1][b];
            //аналогично для i+1 элемента
            b = l1 + 1;
            while (b <= l2 && population[i + 1][j] !== population[i + 1][b]) ++(b);
            if (b !== l2 + 1) population[i + 1][j] = population[i][b];
        }
        //для элементов после l2
        for (let j = l2 + 1; j < 4; ++j) {
            b = l1 + 1;
            while (b <= l2 && population[i][j] !== population[i][b]) ++(b);
            if (b !== l2 + 1) population[i][j] = population[i + 1][b];
            b = l1 + 1;
            while (b <= l2 && population[i + 1][j] !== population[i + 1][b]) ++(b);
            if (b !== l2 + 1) population[i + 1][j] = population[i][b];
        }
    }

    out.append("\r\nПул потомков:\r\n\r\n");
    for (let i = 0; i < crossingover_max_amount; ++i) {
        for (let j = 0; j < 4; ++j) {
            if (child[i][j]) {
                out.append("1 ");
            } else {
                out.append("0 ");
            }
        }
        out.append("\r\n");
        if (i % 2 === 1) out.append("\r\n");
    }
}

//ператор кроссинговера на основе «Золотого сечени
function golden_ratio_cr() {
    out.append("Оператор кроссинговера на основе «Золотого сечения»\r\n");
    let l1, l2; //точка скрещивания
    for (let i = 0; i < crossingover_max_amount; i += 2) { //построение точек скрещивания на основе золотого сечения
        l1 = 1;//первая точка разрыва после первого гена
        l2 = 3;//вторая точка разрыва после второго гена
        //обмен генов
        for (let j = 0; j < l1; ++j) {
            //до первой точки скрещивани€
            child[i][j] = population[population_size + i][j];
            child[i + 1][j] = population[population_size + i + 1][j];
        }
        for (let j = l1; j < l2; ++j) {
            //после первой и до второй точек скрещивани€
            child[i + 1][j] = population[population_size + i][j];
            child[i][j] = population[population_size + i + 1][j];
        }
        for (let j = l2; j < 4; ++j) {
            //после второй точки скрещивани€
            child[i][j] = population[population_size + i][j];
            child[i + 1][j] = population[population_size + i + 1][j];
        }
    }
    out.append("\r\nПул потомков:\r\n\r\n");
    for (let i = 0; i < crossingover_max_amount; ++i) {
        for (let j = 0; j < 4; ++j) {
            if (child[i][j]) {
                out.append("1 ");
            } else {
                out.append("0 ");
            }
        }
        out.append("\r\n");
        if (i % 2 === 1) {
            out.append("\r\n");
        }
    }
    for (let i = 0; i < crossingover_max_amount; ++i) {
        for (let j = 0; j < 4; ++j) {
            population[population_size + i][j] = child[i][j];
        }
    }
}

function mutation_inversion() {
    let c, l1, l2;//номер хромосомы и точки разрыва для мутации
    out.append("Мутация инверсии. Количество мутаций: " + mutation_number + "\r\n");
    for (let i = 0; i < mutation_number; ++i) {
        c = rand(crossingover_max_amount);
        l1 = rand(3);
        do l2 = rand(3);
        while (l2 === l1);
        if(l1 > l2) {
            let temp = l1;
            l1 = l2;
            l2 = temp;
        }
        out.append("Выбрана хромосома номер " + (c + 1)
            + ". Выбрана точки для инверсии: " + (l1 + 1) + ", " + (l2 + 1) +"\r\n");
        //переписываем выбранную хромосому в пул мутаций
        for (let j = 0; j < 4; ++j) {
            population[population_size + crossingover_max_amount + i][j] = population[population_size + c][j];
        }

        let temp;
        // "разворачиваем" отрезок от l1 до l2
        for (let j = l1; l1 + j < l2 - j; j++) {
            temp = population[population_size + crossingover_max_amount + i][l1 + j];
            population[population_size + crossingover_max_amount + i][l1 + j]
                = population[population_size + crossingover_max_amount + i][l2 - j];
            population[population_size + crossingover_max_amount + i][l2 - j] = temp;
        }
    }
}

function mutation_golden_ratio_m() {
    out.append("Мутация инверсии. Количество мутаций: " + mutation_number + "\r\n");
    let c;//номер хромосомы для мутации
    let l1 = [0];//выбираем гены для обмена согласно золотому сечению
    let l2 = [2];//таким образом выбирается 1-й и 3-й гены
    let tmp;//переменная для свапа
    for (let i = 0; i < mutation_number; ++i) {
        c = rand(crossingover_max_amount);
        out.append("Выбрана хромосома номер " + (c + 1)+
            + ". Выбрана точки для инверсии: " + (l1 + 1) + ", " + (l2 + 1) +"\r\n");
        //переписываем выбранную хромосому в пул мутаций
        for (let j = 0; j < 4; ++j)
            population[population_size + crossingover_max_amount + i][j] = population[population_size + c][j];
        //переставл€ем местами гены на золотых точках
        tmp = population[population_size + crossingover_max_amount + i][l1];
        population[population_size + crossingover_max_amount + i][l1] = population[population_size + crossingover_max_amount + i][l2];
        population[population_size + crossingover_max_amount + i][l2] = tmp;
    }
}

let replace = document.getElementsByName('change')[0];
function replace_chromosomes() {
    for (let i = 0; i < population_size + crossingover_max_amount + mutation_number; ++i) {
        if ((decryption(population[i]) < range_left) || (decryption(population[i]) > range_right)) {
            population[i] = [1, 0, 0, 0];
        }
    }
}

let population_size, generation_amount, focus, focus_range, crossingover_probability, mutation_probability, crossingover_max_amount, blanket,
    mutation_number, number_fitness, population_strategy, range_left, range_right, selection, crossingover_operator, population,
    mutation_operator, total_fitness, date_now;

function StartGA() {
    out.innerHTML = '';
    answer.innerHTML = '';
    date_now = new Date();
    // Численность популяции
    population_size = document.getElementsByName('population')[0].value;
    population_size = Math.trunc(population_size);
    if (population_size > 100) {
        document.getElementsByName('population')[0].value = 100;
        population_size = 100;
    }

    // Число генераций
    generation_amount = document.getElementsByName('generation')[0].value;
    generation_amount = Math.trunc(generation_amount);
    if (generation_amount > 100) {
        document.getElementsByName('generation')[0].value = 100;
        generation_amount = 100;
    }
    // Вероятность кроссинговера
    crossingover_probability = document.getElementsByName('crossingover_n')[0].value;
    crossingover_probability = Math.trunc(crossingover_probability);
    if (crossingover_probability > 100) {
        document.getElementsByName('crossingover_n')[0].value = 100;
        crossingover_probability = 100;
    }
    // Вероятность мутации
    mutation_probability = document.getElementsByName('mutation_n')[0].value;
    mutation_probability = Math.trunc(mutation_probability);
    if (mutation_probability > 100) {
        document.getElementsByName('mutation_n')[0].value = 100;
        mutation_probability = 100;
    }
    // Стратегия создания популяции
    population_strategy = document.querySelector('input[name="strategy"]:checked').value;
    //вид селекции
    selection = document.querySelector('input[name="selection"]:checked').value;
    //оператор кроссинговера
    crossingover_operator = document.querySelector('input[name="crossingover"]:checked').value;
    //оператор мутации/инверсии
    mutation_operator = document.querySelector('input[name="mutation"]:checked').value;
    range_left = 8;
    range_right = 12;


    //определение размера родительского пула. Если размер популяции не меньше двух, то кроссинговер возможен
    crossingover_max_amount = 0;
    if (population_size >= 2) {
        crossingover_max_amount = Math.trunc(population_size * crossingover_probability / 100);
        if (crossingover_max_amount % 2 === 1) {
            --crossingover_max_amount;
        }
        if (crossingover_max_amount < 2 && crossingover_probability !== 0) {
            crossingover_max_amount = 2;
        }
    }

    //определение размера пула мутаций. Если кроссинговер невозможен, мутации не происходят
    mutation_number = 0;
    if (crossingover_max_amount) mutation_number = Math.trunc(crossingover_max_amount * mutation_probability / 100);

    // Генерация начальной популяции
    population = new Array(population_size + crossingover_max_amount + mutation_number);
    for (let i = 0; i < population_size + crossingover_max_amount + mutation_number; ++i) {
        population[i] = new Array(4);
    }
    //временный массив потомков
    child = [];
    for (let i = 0; i < crossingover_max_amount; ++i) {
        child[i] = [];
        for (let j = 0; j < 4; ++j) {
            child[i][j] = true;
        }
    }

    // генерация хромосомы
    if (population_strategy === 'focusr') {
        focus_range = document.getElementsByName('radius')[0].value;//величина максимального смещения от фокуса
        focus_range = Math.trunc(focus_range);
        focus = 8 + rand(12);//фокус, находится в области решения задачи
        while (!(focus + focus_range <= 12 && focus - focus_range >= 8)) focus = 8 + rand(12);//весь интервал генерации должен лежать в области решения
        range_left = focus - focus_range;
        range_right = focus + focus_range;
    }
    //случай одеяла
    if (population_strategy === 'blanket') {
        for (let i = 0; i < population_size; ++i) {
            blanket = 8 + i % 12;
            for (let j = 3; j > -1; --j) {
                population[i][j] = blanket % 2;
                blanket = blanket / 2;
            }
        }
    }
    //случай фокусировки
    else if (population_strategy === 'focusr') {
        for (let i = 0; i < population_size; ++i) {
            do {
                for (let j = 0; j < 4; ++j) {
                    population[i][j] = rand(2);
                }
            }
            while (decryption(population[i]) < range_left || decryption(population[i]) > range_right);
        }
    }

    // Вывод начальной популяции
    if (population_strategy === 'focusr') out.append("Выбрана стратегия фокусировки(фокус ="+ focus_range +".\r\n\r\n");
    else if (population_strategy === 'blanket') out.append("Выбрана стратегия Одеяла.\r\n\r\n");
    out.append("Начальная популяция (в скобках указаны фенотип и значение целевой функции):\r\n\r\n");

    for (let i = 0; i < population_size; ++i) {
        for (let j = 0; j < 4; ++j) {
            if (population[i][j]) {
                out.append("1 ");
            }
            else {
                out.append("0 ");
            }
        }
        out.append("(" + (decryption(population[i])) + ", " + (objective_function(decryption(population[i]))) + ")" + "\r\n");
    }

    // начало итераций.
    for (let k = 1; k <= generation_amount; ++k) {
        out.append("---------------------------------------\r\n");
        out.append("Генерация " + k + "\r\n\r\n");
        //селекция
        if (selection === 'random') selection_random();
        else if(selection === 'inbreeding') selection_inbreeding();
        //кроссинговер
        if (crossingover_operator === 'standard_single') standard_single();
        if (crossingover_operator === 'prelevant_single') prelevant_single();
        if (crossingover_operator === 'golden_ratio_cr') golden_ratio_cr();
        //мутация
        if (mutation_operator === 'golden_ratio_m') mutation_golden_ratio_m();
        if (mutation_operator === 'inversion') mutation_inversion();

        //показываем популяцию до отбора
        out.append("\r\n Популяция после кроссинговера и мутации: \r\n\r\n");
        for (let i = 0; i < population_size + crossingover_max_amount + mutation_number; ++i) {
            for (let j = 0; j < 4; ++j) {
                if (population[i][j])  {
                    out.append("1 ");
                }
                else {
                    out.append("0 ");
                }
            }
            out.append("(" + (decryption(population[i])) + ", " + (objective_function(decryption(population[i]))) + ")" + "\r\n");
        }
        if (replace.checked) {
            replace_chromosomes();
        }

        //отсев
        quick_sort(population, 0, population_size + crossingover_max_amount + mutation_number - 1);
        //выводим популяцию и считаем приспособленность
        total_fitness = 0;
        number_fitness = 0;
        out.append("\r\n Популяция после отбора (в скобках указаны фенотип и значение целевой функции): \r\n\r\n");
        for (let i = 0; i < population_size; ++i) {
            for (let j = 0; j < 4; ++j) {
                if (population[i][j]) {
                    out.append("1 ");
                }
                else {
                    out.append("0 ");
                }
            }
            out.append("(" + (decryption(population[i])) + ", " + (objective_function(decryption(population[i]))) + ")" + "\r\n");
            if (!(decryption(population[i]) < range_left || decryption(population[i]) > range_right)) {
                total_fitness += objective_function(decryption(population[i]));
                ++number_fitness;
            }
        }
        if (number_fitness) {
            total_fitness /= number_fitness;
            out.append("\r\n Средняя приспособленность популяции: ");
            out.append(total_fitness);
            out.append("\r\n");
        } else {
            out.append("\r\n В популяции не осталось приспособленных особей.\r\n");
            break;
        }
    }
    //вывод ответа:
    if (number_fitness) {
        answer.append("\r\n Ответ: " + (decryption(population[0])));
    }
    else {
        answer.append("\r\n Популяция вымерла, ответ не найден.");
    }
    answer.append("\r\n Затрачено времени: " + ((new Date()) - date_now) + "ms");
}
